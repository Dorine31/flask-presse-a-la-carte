# poc-flask
Create flask poc to check the possibility of easily creating an api.

## firstly
- install postgresql
- create a postgres user (name : flask, password :1234)
- create a database (name: poc_flask)
- launch flask_app
- init database
- check database content

## launch flask app
```
export FLASK_APP=dev.py
export FLASK_DEBUG=True #debug mode
flask run
```

## Create postgres user and database
```
sudo -u postgres createuser flask -P
createdb poc_flask
```

## init database
```
flask db init
flask db migrate
flask db upgrade
```

## check database content
```
sudo -u postgres psql
\l #databases list
\du #roles list
\c poc_flask #open poc_flask database
\dt #tables list
\d+ subscriptions #columns list
\d or \q #quit
```

## import the pdf models
Actually, the files are stored locally. The path is hard coded, cf. routes => line 262 (searchPdfPaths())
You need to add them manually and click on the button "ajouter un nouveau modèle" to save them into the database.
The endpoint API which link the button : cf. routes => line 181 (create_model())