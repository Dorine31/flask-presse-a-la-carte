from database import db

#import logging, sys
#logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

# TODO modify the name : firstname and lastname
# TODO create a rubric class, a subscription_type class
# or transform it into a string
class Subscription(db.Model):
    __tablename__ = 'subscriptions'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False)
    address = db.Column(db.String, nullable=False)
    age = db.Column(db.Integer, nullable=False)
    rubrics = db.Column(db.ARRAY(db.String), nullable=False)
    subscription_type = db.Column(db.ARRAY(db.String), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __init__(self, name, email, address, age, rubrics, subscription_type, user_id):
        self.name = name
        self.email = email
        self.address = address
        self.age = age
        self.rubrics = rubrics
        self.subscription_type = subscription_type
        self.user_id = user_id

    @staticmethod
    def get_subscription(id):
        subscription = db.session.query(Subscription).filter(Subscription.id == id).first()
        return subscription or []

    # return a list of all subscriptions
    @staticmethod
    def get_all_subscriptions():
        subscriptions = db.session.query(Subscription).all()
        return subscriptions or []
    
    # return a list of user subscriptions
    @staticmethod
    def get_subscriptions(user_id):
        subscriptions = db.session.query(Subscription).filter(Subscription.user_id == user_id)
        return subscriptions or []

    @staticmethod
    def create(name, email, address, age, rubrics, subscription_type, user_id):
        new_model = Subscription(name, email, address, age, rubrics, subscription_type, user_id)
        exists = db.session.query(db.exists().where(Subscription.name == name)).scalar()
        if exists:
            return {}
        db.session.add(new_model)
        db.session.commit()
        return new_model

    @staticmethod
    def modifyOne(id, name, email, address, age, rubrics, subscription_type, user_id):
        subscription = Subscription.get_subscription(id)
        subscription.name = name
        subscription.email = email
        subscription.address = address
        subscription.age = age
        subscription.rubrics = rubrics
        subscription.subscription_type = subscription_type
        subscription.user_id = user_id
        db.session.commit()
        return 'ok'

    @staticmethod
    def delete_one(id):
        subscription = Subscription.query.get(id)
        db.session.delete(subscription)
        db.session.commit()
   
    @staticmethod
    def delete_all():
        num_rows_deleted = db.session.query(Subscription).delete()
        db.session.commit()
        return num_rows_deleted

    def __repr__(self):
        return f"{self.subscription_type}"