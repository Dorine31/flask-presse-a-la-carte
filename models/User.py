
from database import db
from werkzeug.security import check_password_hash

#import logging, sys
#logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, nullable=False)
    password = db.Column(db.String, nullable=False)
    isAdmin = db.Column(db.Boolean, nullable=False)
    
    def __init__(self, email, password, isAdmin):
        self.email = email
        self.password = password
        self.isAdmin = isAdmin
        self.subscriptions = []

    @staticmethod
    def checkUser(email):
        user = db.session.query(User).filter(User.email == email).first() # if this returns a user, then the email already exists in database
        return user or {}
    
    @staticmethod
    def getUser(id):
        user = db.session.query(User).filter(User.id == id).first()
        return user or {}
    
    @staticmethod
    def getUsers():
        users = [{'id': i.id, 'email': i.email, 'isAdmin': i.isAdmin}
                for i in User.query.order_by('id').all()]
        return users or []
    
    @staticmethod
    def createUser(email, password):
        user = User(email, password, False)
        exists = db.session.query(db.exists().where(User.email == email)).scalar()
        if exists:
            return {}
        db.session.add(user)
        db.session.commit()
        return user
    
    @staticmethod
    def login(email, password):
        user = db.session.query(User).filter(User.email == email).first()
        if user:
            is_equal = check_password_hash(user.password, password)
            if is_equal:
                return user 
            else:
                return {}
        else:
            return {}

    @staticmethod
    def modifyOne(id, email, isAdmin):
        user = User.getUser(id)
        user.email = email
        user.isAdmin = isAdmin
        db.session.commit()
        return 'ok'
