from database import db
#import logging, sys
#logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

class ModelPDF(db.Model):
    __tablename__ = 'pdfmodels'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    path = db.Column(db.String, nullable=False)

    def __init__(self, name, path):
        self.name = name
        self.path = path

    @staticmethod
    def create(name, path):
        new_model = ModelPDF(name, path)
        exists = db.session.query(db.exists().where(ModelPDF.name == name)).scalar()
        if exists:
            return {}
        db.session.add(new_model)
        db.session.commit()
        return new_model

    @staticmethod
    def delete_all():
        num_rows_deleted = db.session.query(ModelPDF).delete()
        db.session.commit()
        return num_rows_deleted

    @staticmethod
    def get_pdfmodels():
        return [{'id': i.id, 'name': i.name, 'path': i.path}
                for i in ModelPDF.query.order_by('id').all()]
    
    def __repr__(self):
        return f"<ModelPDF {self.name}"
