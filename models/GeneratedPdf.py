
import datetime
import os
import base64
from database import db
from models import Subscription
from pypdf import PdfMerger
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from sendgrid.helpers.mail import (Mail, Attachment, FileContent, FileName, FileType, Disposition, ContentId)

# import logging, sys, os
# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

class GeneratedPDF(db.Model):
    __tablename__ = 'generated_pdf'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    path = db.Column(db.String, nullable=False)
    created_at = db.Column('created_at', db.Date, default=datetime.datetime.now())
    subscription_id = db.Column(db.Integer, db.ForeignKey('subscriptions.id', ondelete='CASCADE'))

    def __init__(self, name, path, created_at, subscription_id):
            self.name = name
            self.path = path
            self.created_at = created_at
            self.subscription_id = subscription_id

    # get the generated pdf
    @staticmethod
    def get_files():
        return [{'id': i.id, 'name': i.name, 'path': i.path, 'created_at': i.created_at, 'subscription_id': i.subscription_id}
                for i in GeneratedPDF.query.order_by('id').all()]

    @staticmethod
    def create(name, path, created_at, subscription_id):
        new_model = GeneratedPDF(name, path, created_at, subscription_id)
        exists = db.session.query(db.exists().where(GeneratedPDF.name == name)).scalar()
        if not exists:
            db.session.add(new_model)
            db.session.commit()
            return new_model

    @staticmethod
    def delete_all():
        num_rows_deleted = db.session.query(GeneratedPDF).delete()
        db.session.commit()
        return num_rows_deleted

    def __repr__(self):
        return f'<GeneratedPDF "{self.name}">'

    def createNewFiles(subscriptions, templatesPath):
        paths = []
        for i, subscription in enumerate(subscriptions):
                filesToMerge = concatTheFilesToMerge(subscription, templatesPath)
                # check if the directory exists
                directory = "static/files/"
                if not os.path.exists(directory):
                    os.makedirs(directory)
                path = directory + subscription.name + "_" + str(subscription.id) + "_" + str(datetime.datetime.now())[:10] + ".pdf"
                merger = PdfMerger()
                for pdf in filesToMerge:
                    merger.append(pdf)
                merger.write(path)
                paths.append(path)
                merger.close()
        return paths

    def send_digital_subscriptions(path):
            # check the subscription type
            subscriptionId = path.split('_')[-2]
            if subscriptionId:
                subscription = Subscription.get_subscription(subscriptionId)
                try:
                    types = subscription.subscription_type
                    email = subscription.email
                    if 'digital' in types:
                        # send email
                        sendEmail(email, path)
                except:
                    print('erreur')

def concatTheFilesToMerge(subscription, models):
    filesToMerge = []
    for rubric in subscription.rubrics:
        # create a rubric name
        if rubric == '' or rubric == None:
            return []
        rubric = rubric.lower().strip()
        name = rubric
        pubName = 'pub-' + rubric + '-child' if int(subscription.age) < 12 else 'pub-' + rubric
        # retrieve the path and populate the filesToMerge array
        for model in models:
            for key, value in model.items():
                if key == 'name' and value == name:
                    path = model['path']
                    filesToMerge.append(path)
                # do the same for the adds
                if key == 'name' and value == pubName:
                    pubPath = model['path']
                    filesToMerge.append(pubPath)
    return filesToMerge

def sendEmail(email, file_path):
    message = Mail(
        from_email='dorine.berton@epsi.fr',
        to_emails=email,
        subject='Envoi de votre abonnement',
        html_content='<strong>Bonjour, voici le journal commandé. Cordialement. Presse à la carte</strong>'
    )
    with open(file_path, 'rb') as f:
        data = f.read()
    encoded = base64.b64encode(data).decode()
    attachment = Attachment()
    attachment.file_content = FileContent(encoded)
    attachment.file_type = FileType('application/pdf')
    attachment.file_name = FileName(file_path.split('/')[-1])
    attachment.disposition = Disposition('attachment')
    attachment.content_id = ContentId('Example Content ID')
    message.attachment = attachment
    try:
        key = os.getenv('SENDGRID_API_KEY')
        sg = SendGridAPIClient(key)
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as error:
        print(EOFError.message)