from flask import Flask
from routes import *

from models import *
from config import Config
from database import db

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    app.register_blueprint(index)
    app.register_blueprint(user)
    app.register_blueprint(auth)
    app.register_blueprint(pdf)
    app.register_blueprint(subscription)

    return app