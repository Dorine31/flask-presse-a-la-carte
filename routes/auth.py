from models import Subscription, User
from flask import flash, request, render_template, redirect, url_for, session
from werkzeug.security import generate_password_hash
from . import auth
# import logging, sys

# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

@auth.route('/login')
def login():
    session['admin'] = False
    return render_template('login.html')

@auth.route('/login', methods=['POST'])
def login_post():
    if request.method == "POST":
        email = request.form.get('email')
        password = request.form.get('password')
        user = User.login(email, password)
        if user:
            session['user'] = user.id
            if  user.isAdmin == False:
                session['admin'] = False
                subscriptions = Subscription.get_subscriptions(user.id)
                return redirect(url_for("user.profile", id=user.id))
            else:
                session['admin'] = True
                User.getUsers()
                Subscription.get_all_subscriptions()
                return redirect(url_for('user.displayUsers'))
        else:
            flash('Email ou mot de passe erroné', 'error')
            return redirect(url_for('auth.login'))

@auth.route('/signup')
def signup():
    return render_template('register.html')

@auth.route('/signup', methods=['GET', 'POST'])
def signup_post():
    if request.method == "POST":
        email = request.form.get('email')
        password = request.form.get('password')
        password = generate_password_hash(password, method='sha256')
        newUser = User.createUser(email, password)
        if(newUser):
            return render_template('login.html')
        else:
            return render_template('register.html')

@auth.route('/logout')
def logout():
    session['user'] = None
    session['admin'] = False
    return redirect(url_for('index.home'))
