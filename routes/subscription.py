from models import Subscription, ModelPDF, GeneratedPDF, User
from flask import request, render_template, redirect, url_for, session
from . import subscription
import datetime
# import logging, sys

# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

@subscription.route('/displaySubscriptions')
def displaySubscriptions():
    subscriptions = Subscription.get_all_subscriptions()
    return render_template('displaySubscriptions.html', userIsActive=False, subscriptions=subscriptions)
    
@subscription.route('/modifySubscription/<int:id>')
def modifySubscription(id):
    subscription = Subscription.get_subscription(id)
    rubrics = {}
    types = {}
    for rubric in subscription.rubrics:
        rubrics[rubric] = True
    for type in subscription.subscription_type:
        types[type] = True
    if subscription:
        return render_template('modifySubscription.html', types=types, rubrics=rubrics, subscription=subscription)
    else:
        subscriptions = Subscription.get_all_subscriptions()
        return render_template('displaySubscriptions.html', userIsActive=False, subscriptions=subscriptions)
    
@subscription.route('/modifySubscription/<int:id>', methods=['PUT', 'POST'])
def modify_subscription(id):
    if request.method == "POST":
        name = request.form.get('name')
        email = request.form.get('email')
        address = request.form.get('address')
        age = request.form.get('age')
        rubrics = request.form.getlist('rubrics')
        digital = request.form.get('digital')
        paper = request.form.get('paper')
        user_id = request.form.get('userId')
        types = []
        if (digital != None) or (paper == None):
            types.append('digital')
        if paper != None:
            types.append('paper')
        subscription = Subscription.modifyOne(id, name, email, address, age, rubrics, types, user_id)
        if subscription:
            subscriptions = Subscription.get_all_subscriptions()
            return render_template('displaySubscriptions.html', userIsActive=False, subscriptions=subscriptions)
        else:
            return render_template('/modifySubscription/<int:user_id>')

@subscription.route('/deleteSubscription/<int:id>')
def deleteSubscription(id):
    try:
        Subscription.delete_one(id)
    except:
        print('impossible supprimer')
    subscriptions = Subscription.get_all_subscriptions()
    return render_template('displaySubscriptions.html', userIsActive=False, subscriptions=subscriptions)

# create news
@subscription.route("/edit_subscriptions", methods=["GET", "POST"])
def edit_subscriptions():
    if request.method == "POST":
        templatesPath = []
        templatesPath = ModelPDF.get_pdfmodels()
        subscriptions = Subscription.get_all_subscriptions()
        paths = GeneratedPDF.createNewFiles(subscriptions, templatesPath)
        for path in paths:
            date = datetime.datetime.now()
            GeneratedPDF.create(path.split('/')[-1], path, date, path.split('_')[-2])
            # send all digital subscriptions by email
            GeneratedPDF.send_digital_subscriptions(path)
        return render_template('created.html', paths=paths)

# send form - create a subscription
@subscription.route("/createSubscription", methods=['GET', 'POST'])
def createSubscription():
    if session.get('user'):
        user = session['user']
        if request.method == 'POST':
            name = request.form.get('name')
            email = request.form.get('email')
            address = request.form.get('address')
            age = request.form.get('age')
            rubrics = request.form.getlist('rubrics')
            digital = request.form.get('digital')
            paper = request.form.get('paper')
            user_id = session['user']
            types = []
            if (digital != None) or (paper == None):
                types.append('digital')
            if paper != None:
                types.append('paper')
            model = Subscription(name, email, address, age, rubrics, types, user_id)
            if model:
                subscription = Subscription.create(name, email, address, age, rubrics, types, user_id)
            if subscription:
                subscriptions = Subscription.get_subscriptions(user_id)
                user = User.getUser(user_id)
                return render_template('profile.html', user=user, subscriptions=subscriptions)
        else:    
            return render_template('createSubscription.html')
    else:
        return redirect(url_for('auth.login'))
