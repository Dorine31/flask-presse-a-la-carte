from flask import Blueprint
index = Blueprint('index', __name__) 
auth = Blueprint('auth', __name__) 
user = Blueprint('user', __name__) 
subscription = Blueprint('subscription', __name__) 
pdf = Blueprint('pdf', __name__) 

from .index import *
from .auth import *
from .user import *
from .subscription import *
from .pdf import *
