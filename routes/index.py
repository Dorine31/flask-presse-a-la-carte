from . import index

from flask import render_template, redirect, url_for, session
# import logging, sys

# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

@index.route("/")
def home():
    if session.get('user'):
        if session['admin'] == True:
            return redirect(url_for('user.displayUsers'))
        else:
            return redirect(url_for('user.profile', id=id))
    return render_template('home.html')
