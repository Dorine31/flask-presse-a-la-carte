from models import ModelPDF, GeneratedPDF
from flask import render_template
from . import pdf
# import logging, sys
import os
import glob

# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

@pdf.route("/getNewFiles")
def get_new_files():
    files = GeneratedPDF.get_files()
    return render_template('newfiles.html', data=files)

# create a pdf model (new or ads)
# Pay attention, to save the pdf model into the database, using the create_model function, 
# you need to add manually the file into the "target" folder (see the function below)
# After that, you will be able to click on the displaySubscriptions html button named "ajouter un nouveau modèle"
# TODO create an import/export file to replace it
@pdf.route("/createModel")
def create_model():
    arr = searchPdfPaths()
    if arr:
        messages = []
        for model in arr:
            for name, path in model.items():
                result = ModelPDF.create(name, path)
                message = name + ' existe déjà' if result == {} else result.name + 'sauvegardé'
                messages.append(message)
        print(len(messages))
        return render_template('modelAdded.html', messages=messages)
    else:
        return render_template('home.html')
    
# EXTERNAL FUNCTION
def searchPdfPaths():
        # Pay attention, the link is hard coded
        target = "/home/dorine/4-epsi/OPEN-INNOVATION/annee2/modeles-pdf/**/*.pdf"
        files = glob.glob(target, recursive=True)
        models = []
        for file in files:
            model = {}
            name = os.path.basename(file).split('.', 1)[0]
            model[name] = file
            models.append(model)
        return models
