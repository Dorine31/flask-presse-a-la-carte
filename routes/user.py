from models import Subscription, User
from flask import flash, request, render_template, redirect, url_for, session
from . import user
# import logging, sys
import ast

# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

# page edit user
@user.route('/modifyUser/<int:id>')
def modify(id):
    user = User.getUser(id)
    if user:
        return render_template('modifyUser.html', user=user)

# ADMIN DASHBOARD
@user.route('/displayUsers')
def displayUsers():
    users = User.getUsers()
    return render_template('displayUsers.html', userIsActive=True, users=users)

# send form edit user
@user.route('/modifyUser/<int:id>', methods=['PUT', 'POST'])
def modify_user(id):
    if request.method == "POST":
        email = request.form.get('email')
        isAdmin = request.form.get('isAdmin')
        try:
            User.modifyOne(id, email, ast.literal_eval(isAdmin))
            users = User.getUsers()
            subscriptions = Subscription.get_all_subscriptions()
            return render_template('displayUsers.html', userIsActive=True, users=users, subscriptions=subscriptions)
        except:
            flash('Utilisateur non modifié', 'error')
            return 'erreur'
    
# SUBSCRIPTER DASHBOARD
# send form edit user profile
@user.route('/modifyProfile/<int:id>', methods=['PUT', 'POST'])
def modify_profile(id):
    if request.method == "POST":
        user_id = session['user']
        email = request.form.get('email')
        isAdmin = request.form.get('isAdmin')
        if id == int(user_id):
            try:
                user = User.modifyOne(id, email, ast.literal_eval(isAdmin))
                flash('Email modifié', 'success')
            except:
                user = User.getUser(id)
                flash('Email non modifié', 'error')
            return redirect(url_for('user.profile', id=id))
        subscriptions = Subscription.get_subscriptions(id)
        return render_template('profile.html', user=user, subscriptions=subscriptions)    

# page profile
@user.route('/profile/<int:id>')
def profile(id):
    try:
        user_id = session['user']
        if id == int(user_id):
            user = User.getUser(id)
            subscriptions = Subscription.get_subscriptions(id)
            return render_template('profile.html', user=user, subscriptions=subscriptions)
    except:
        session['user'] = None
        session['admin'] = False
        return render_template('home.html')
